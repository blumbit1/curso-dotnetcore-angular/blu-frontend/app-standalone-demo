import { Injectable } from '@angular/core';
import { EmployeeInfo } from '../interface/employee-info.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public employeeList: EmployeeInfo[] = [];

  ngOnInit(){
    this.employeeList = [
      { "employeeId": 1, "employeeName": "Roger Smith", "employeeCode" : "ET0002334", "employeeMobile" : 77712345, "employeeEmail": "roger.smith@yahoo.com"},
      { "employeeId": 2, "employeeName": "Alex Bob", "employeeCode" : "ET0002992", "employeeMobile" : 76229585, "employeeEmail" : "alex.bob@gmail.com"},
      { "employeeId": 3, "employeeName": "Stephen Ken", "employeeCode" : "ET0001675", "employeeMobile" : 71241312, "employeeEmail" : "stephen.123@yahoo.com"},
      ];
  }

  getEmployeeList()  {
    if (this.employeeList.length == 0 )
        this.ngOnInit();
    return this.employeeList;
  }

  initializeData(){
    this.getEmployeeList();
  }

  getEmployee(id : number){
    return this.employeeList.find(e=>e.employeeId == id);
  }

  addEmployeeInfo(emloyee : EmployeeInfo){
    this.employeeList.push(emloyee);
  }
}
