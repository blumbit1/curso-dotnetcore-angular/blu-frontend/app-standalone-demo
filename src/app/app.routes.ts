import { Routes } from '@angular/router';
import { EmployeeIndexComponent } from './employee/employee-index.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { EmployeeDetailComponent } from './employee-details/employee-detail.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: EmployeeIndexComponent },
  { path: 'employee-index', component: EmployeeIndexComponent },
  { path: 'employee-add', component: EmployeeAddComponent },
  { path: 'employee-view/:id', component: EmployeeDetailComponent }
];
