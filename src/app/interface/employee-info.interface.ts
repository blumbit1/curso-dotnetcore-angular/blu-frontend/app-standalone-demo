export interface EmployeeInfo {
    employeeId : number;
    employeeCode : string;
    employeeName : string;
    employeeMobile : number;
    employeeEmail : string;
}
